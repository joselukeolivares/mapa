var chart=treemapChart()
function build_treeMap(year){

    var _data=treeMapData[year]
    /*
    d3.json("treeMap/flare.json",function(nodes){
        chart.nodes(nodes).valueAccesor(size).render()
    })
    */
   childs_Obj_toArrays(_data)
   /*
    Object.keys(_data).forEach(key=>{
        if(key==="children"){
            _data.children=remakeData(_data.children)
        }
    })
   */ 
    //
    chart.nodes(_data).valueAccesor(size).render()
    
    
}

function childs_Obj_toArrays(parentObj){
    Object.keys(parentObj).forEach(key=>{
        if(key==="children"){
            parentObj.children=remakeData(parentObj.children)
        }
    })

}

function remakeData(data){
var auxArray=[]
    Object.keys(data).forEach(key=>{
        auxArray.push(data[key])
        data[key].child_number=auxArray.length-1       

    })

return auxArray

}

function treemapChart(){
    var _chart={}

    var _width=800,_height=400,
    _colors=d3.scaleOrdinal(d3.schemaCategory20c),
    _svg,
    _nodes,
    _valueAccesor,
    _treemap,
    _backRect,
    _history=[{letter:"Click dentro del cuadro para zoom"}],
    _data_auxliar,
    _bodyG;

    _chart.render=function(){
        
        if(!_svg){
            _svg=d3.select("#treemapChart")
                    .append("svg")
                    .attr("id","treemap")
                    .attr("width",_width)
                    .attr("height",_height)

            if(!_data_auxliar)
            _data_auxliar=_nodes        
        }
        

        renderBody(_svg)
    }

    function renderBody(svg){
        if(!_bodyG){
            _bodyG=svg.append("g")
            .attr("class","body")

   

            if(!_backRect){
                _backRect=document.getElementById("backMenu")
                _backRect.addEventListener("click",backLevel)
            }

            

        }
        _treemap=d3.treemap() //--->(A)
        .size([_width,_height])
        .round(true)
        .padding(1)

        var root=d3.hierarchy(_nodes)//---B
                   .sum(_valueAccesor)
                   .sort(function(a,b){return b.value -a.value})

         _treemap(root)//---C
         
         var cells=_bodyG.selectAll("g")
                    .data(root.leaves(),(d)=>{
                        //console.log(d.data.name)
                        return d.data.name //---D
                    })

         renderCells(cells)           
    }


    function renderCells(cells){
        var cellEnter=cells.enter().append("g")
                        .merge(cells)
                        .attr("class","cell")
                        .attr("transform",d=>{
                            return "translate("+d.x0+","+d.y0+")" //--->E
                        })

        renderRect(cellEnter,cells)
        renderText(cellEnter,cells)                
        cells.exit().remove()
    }

    function  renderRect(cellEnter,cells){
        cellEnter.append("rect")
        .attr("class","rect_treeMap")
        .on("click",function(d){
            
            changeLevel(d)
               })
                 
        cellEnter.merge(cells)                 
                    .transition()
                    .select("rect.rect_treeMap")
                    .attr("width",d=>{
                       
                        var name=d.data.name
                        //console.log(name)
                        return d.x1-d.x0})
                    .attr("height",d=>d.y1-d.y0)
                    .style("fill",//d=>{                                                return _colors(d.parent.data.name)}
                    "rgb(49, 130, 189)")
                    
    }

    function changeLevel(d){
        
        if(Array.isArray(d.data.children)){
            d.children=d.data.children
        }else if(!d.data.children){
            d.children=d.data.children2
        }else{
            
            childs_Obj_toArrays(d.data)            
            d.children=d.data.children
        }
       
        var index=_history.length
        var letters=_history[index-1].letter+">"+d.data.name
        
    
        if(d.children.length!=0){
            _history.push({letter:letters,key:d.data.child_number})
            _backRect.innerHTML=letters
            console.log(d)
            _chart.nodes(d).valueAccesor(size).render()
        }
        
    }

    function backLevel(){
        var index=_history.length
        
        if(index>1){         
            
            
            if(_nodes.data){
                _nodes.data.children2=_nodes.children
                delete _nodes.data["children"]
                                
            }if(_nodes.children){
                _nodes.children2=_nodes.children
                delete _nodes["children"]
                
                
            }
            

            _history.pop()
             
            if(_history.length==1){
                newData=_data_auxliar
            }else{
                for(var i=1;i<_history.length;i++){
                    newData=_data_auxliar.children[_history[i].key]
                                                           
                    for(var j=0;j<newData.children.length;j++){
                        var children=newData.children[j]
                        children.children2={child:newData.children}
                        delete children['children']
                    }
                    
                    
                }
                
            }
            
            index-=2
            //
            _backRect.innerHTML=_history[index].letter

              if(newData!=null){
                  console.log(newData)
                  _chart.nodes(newData).valueAccesor(size).render()     
            
              }               
            
        }
    }

    function renderText(cellEnter,cells){
        cellEnter.append("text")

        cellEnter.merge(cells)
                .select("text")
                .style("font-size",11)
                .attr("x",d=>(d.x1-d.x0)/2)
                .attr("y",d=>(d.y1-d.y0)/2)
                .attr("text-anchor","middle")
                .text(d=>{
                    
                    return d.data.name//+":"+(((d.value/d.parent.value)*100).toFixed(0))+"%"
                })
                .style("opacity",function(d){
                    d.w=this.getComputedTextLength()
                    return d.w<(d.x1 - d.x0) ? 1:0 //--->I
                })
    }

    _chart.width=function(w){
        if(!arguments.length) return _width
        _width=w
        return _chart
    }

    _chart.height=function(h){
        if(!arguments.length)return _height
        _height=h
        return _chart
    }

    _chart.colors=function(c){
        if(!arguments.length)return _colors
        _colors=c
        return _chart
    }

    _chart.nodes=function(n){
        if(!arguments.length)return _nodes
        //console.log(_nodes)
        _nodes=n
        return _chart
    }

    _chart.valueAccesor=function(fn){
        if(!arguments.length)return _valueAccesor
        _valueAccesor=fn
        return _chart
    }

    return _chart


}



function size(d){
    return d.venta_rm_total
}