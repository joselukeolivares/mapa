var states={}
var second_level={}
var circle_toolTip=document.getElementById("circle_tooltip");
var centered

var projection = d3.geoMercator()
    .scale(1500)
    .center([-102.34034978813841, 24.012062015793])
    .translate([width/2,height/2])


     var path = d3.geoPath() // <- A
            .projection(projection);         
             
    var svg=d3.select("#map_mex")
              .append("svg")
              .attr("width",width)
              .attr("height",height)
              .on("click",stoped,true)
    
    var rect_svg=svg.append("rect")
              .attr("class","background")
              .attr("width",width)
              .attr("height",height)
              .attr("style","fill:none;pointer-events:all")
              .on('click',zoomOut)
              
              
    var g=svg.append('g')
             .call(d3.zoom()
                     //.scaleExtent([1,100])
                     
                     .on("zoom",zoomHandler)
                     )
    function stoped(){
      
      if(d3.event.defaultPrevented)d3.stopPropragation()
    }                 

    function zoomOut(d) {
  var x, y, k;

  if (d && centered !== d) {
    var centroid = path.centroid(d);
    x = centroid[0];
    y = centroid[1];
    k = 4;
    centered = d;
  } else {
    x = width / 2;
    y = height / 2;
    k = 1;
    centered = null;
  }

  

  var group=d3.select("#group_in_map")
  group
      .classed("active", centered && function(d) { return d === centered; });
  
      

      group.transition()
      .duration(750)
      .attr("transform", "scale(" + 1 + ") translate(" + (-(0)) + "," + -(0) + ")")
      .style("stroke-width", 1.5 / k + "px");
  
  secondLevel_data(stores_bySelection)
  d3.selectAll(".pin").attr("r",10)
   
}


function secondLevel_data(newstores){

          second_level={}
          for(var i=0;i<newstores.length;i++){
                          
            if(newstores[i]!=null){
                var state=null
                var store=newstores[i]

                if(second_level[store.id_ciudad]){

                    state=second_level[store.id_ciudad]
                }else{
                    state=second_level[store.id_ciudad]={}
                    state.fecha=store.fecha
                    state.nombre_estado=store.nombre_ciudad
                    state.id_ciudad=store.id_ciudad 
                    state.id_estado=store.id_estado                    
                    state.location=store.location
                    state.venta_muebles_total=0
                    state.venta_muebles_credito=0
                    state.venta_muebles_contado=0
                    state.venta_ropa_total=0
                    state.venta_ropa_credito=0
                    state.venta_ropa_contado=0     
                    state.venta_rm_total=0
                    state.numberStores=0
                    state.formatStores={}
                    }

                    
                    var venta_muebles_total=store.venta_muebles_total!=null?store.venta_muebles_total:0
                    var venta_muebles_credito=store.venta_muebles_credito!=null?store.venta_muebles_credito:0
                    var venta_muebles_contado=store.venta_muebles_contado!=null?store.venta_muebles_contado:0
                    var venta_ropa_total=store.venta_ropa_total!=null?store.venta_ropa_total:0
                    var venta_ropa_credito=store.venta_ropa_credito!=null?store.venta_ropa_credito:0
                    var venta_ropa_contado=store.venta_ropa_contado!=null?store.venta_ropa_contado:0
                    

                    state.venta_muebles_total+=venta_muebles_total
                    state.venta_muebles_credito+=venta_muebles_credito
                    state.venta_muebles_contado+=venta_muebles_contado
                    state.venta_ropa_total+=venta_ropa_total
                    state.venta_ropa_credito+=venta_ropa_credito
                    state.venta_ropa_contado+=venta_ropa_contado
                    state.venta_rm_total=state.venta_ropa_total+state.venta_muebles_total
                    state.numberStores++
                    
                    if(state.formatStores[store.formato_tienda]){
                      var state_storeKind=state.formatStores[store.formato_tienda]
                      state_storeKind.number++
                      state_storeKind.venta_muebles_total+=venta_muebles_total
                      state_storeKind.venta_muebles_credito+=venta_muebles_credito
                      state_storeKind.venta_muebles_contado+=venta_muebles_contado
                      state_storeKind.venta_ropa_total+=venta_ropa_total
                      state_storeKind.venta_ropa_credito+=venta_ropa_credito
                      state_storeKind.venta_ropa_contado+=venta_ropa_contado
                      state_storeKind.venta_rm_total=venta_ropa_total+venta_muebles_total
                      
                    
                    }else{

                      state.formatStores[store.formato_tienda]={
                        number:1,
                        venta_muebles_total:venta_muebles_total,
                        venta_muebles_credito:venta_muebles_credito,
                        venta_muebles_contado:venta_muebles_contado,
                        venta_ropa_total:venta_ropa_total,
                        venta_ropa_credito:venta_ropa_credito,
                        venta_ropa_contado:venta_ropa_contado,
                        venta_rm_total:venta_ropa_total+venta_muebles_total
                    
                      
                      }
                    }
            }
          }//for8
            var cities=[]
            

            Object.keys(second_level).forEach(key=>{
              var datum=second_level[key]
              
              cities.push(datum)
            })

            update_data(cities)
            
          

}

function clickedTOTO(d) {

  
  let selected={state_code:d.properties.state_code}  

  for(var i=0;i<stores_data_mx_tj.length;i++){
    if(selected.state_code==stores_data_mx_tj[i].inegi_code){
      
      selected.state_code_coppel=stores_data_mx_tj[i].IdEstado
      i=stores_data_mx_tj.length
    }
  }
 
  
  var stores_bySelection=[]
  
  Object.keys(newstores).forEach(key=>{
    
     var store=newstores[key]
     if(store.id_estado==selected.state_code_coppel){
        stores_bySelection.push(store)
     }
 })
    
    
  
  

  d3.selectAll('.municipalities')
  .classed('invisible',true)

  d3.selectAll('.municipalities.invisible.m'+selected.state_code)
              .classed('invisible',false)
              .style('fill','#97A8CD')



  
              
  var x, y, k;

  if (d && centered !== d) {
    var centroid = path.centroid(d);
    x = centroid[0];
    y = centroid[1];
    k = 4;
    centered = d;
  } else {
    x = width / 2;
    y = height / 2;
    k = 1;
    centered = null;
  }

  var group=d3.select("#group_in_map")
  group
      .classed("active", centered && function(d) { return d === centered; });
  
      

      group.transition()
      .duration(750)
      .attr("transform", "scale(" + k + ") translate(" + (-(x*.7)) + "," + -(y*.8) + ")")
      .style("stroke-width", 1.5 / k + "px");
  
  secondLevel_data(stores_bySelection)
  d3.selectAll(".pin").attr("r",2)         
      
}
             
             

    var circles=null
    var stores=[]
    var center;
    var tooltip=document.getElementById("tooltip_map")
    var state_name=document.getElementById("tooltip_edo")

    function tooltip_mapOn(d){
        console.log("On:"+on)
        on++
       
        tooltip.style.display='flex'
        var coords=d3.mouse(this)        
        tooltip.style.top=(coords[1]+15)+"px"
        tooltip.style.left=(coords[0]+15)+"px"
        state_name.innerHTML=d.properties.state_name!=null?d.properties.state_name:d.properties.mun_name
        console.log(state_name.textContent)

        

    }
    var on=0,off=0

    function tooltip_mapOff(d){
      console.log("Off:"+off)
      off++
      tooltip.style.display='none'
     
    }

    function clicked(d){
      var x=0
      var y=0
      //console.log(d);
      
      if(!d||center===d){
          center=null
          
         
      }else{
        var centroid=path.centroid(d)
         x=(width/2)-centroid[0]
         y=(height/2)-centroid[1]
         center=d

      }

      
      g.transition().duration(1000)
        .on('end',()=>{
                  
                  console.log("state?code="+d.properties.state_code)
                  window.location.href="state?code="+d.properties.state_code})
        .attr("transform","translate("+(x)+","+(y)+")")

      
    }                 
    var storesForState=[]

    var stores_data_mx_tj

    d3.csv("tiendas.csv",(err,data)=>{
      stores_data_mx_tj=data
      data.forEach(element => {
        let store={
              id: element.IdTienda,
              desc:	element.DescTienda,
              idCity: element.IdCiudad,
              descCity:	element.DescCiudad,
              idState:element.IdEstado,
              descState:element.DescEstado,
              inegi_code:element.inegi_code,		
                location: {
                  latitude: element.Latitud,
                  longitude: element.Longitud
                }}
                
             if(store.location.latitude!='NULL' && store.location.longitude!='NULL'){
                stores.push(store)

                      if(!storesForState[store.idState]){
                        if(store.idState!=null){
                            
                            storesForState[store.idState]={
                            idState:store.idState,
                            descState:store.descState,
                            numberStores:1
                            }
                        }

                      }else{
                        if(store.idState!=null)
                        storesForState[store.idState].numberStores++;
                        storesForState[store.idState].location=store.location
                      }
             }   
                
          
          

          
      });

      

    })
   
    function build_country(){

        d3.json("mx_tj.json",function(err,data){
          
       var group=g.append("g").attr("id","group_in_map")

       group.selectAll("path.state")
                .data(topojson.object(data,data.objects.states).geometries)    
                .enter()
                    .append("path")
                    .attr("class",(d,i)=>
                    {
                      console.log((d.properties.state_name+";"+d.properties.state_code+";"))
                    return "state "+(d.properties.state_name).replace(' ','_')})
                    .attr("d",path)
                    .on('click',clickedTOTO)
                    .on('mouseover',tooltip_mapOn)
                    .on('mouseout',tooltip_mapOff)
            
                    
                    group.selectAll("path.municipalities")
              .data(topojson.object(data,data.objects.municipalities).geometries)    
              .enter()
                  .append("path")
                  .attr("class",(d,i)=>
                  {
                    //console.log((d.properties.state_name+";"+d.properties.state_code+";"))
                  return "municipalities invisible m"+(d.properties.state_code)
                  })
                  .attr("d",path)
                  //.on('click',clickedTOTO)
                  //.on('mouseover',tooltip_mapOn)
                  //.on('mouseout',tooltip_mapOff)
                  
    
    
                      
        var newstores=storesForState.filter(d=>d!=null)
        storesForState=null; 
    
          
          
                              
                                   
        update_map(true);
        
        })

    }
    
    var table
    function build_miniTable(data){

  var columns = ["number","formato", "venta_ropa_total", "venta_muebles_total", "venta_rm_total"];
  
    
        var thead,tbody

       
      //
    if(!table){
        table=d3.select("#table_container")
                .append('table')
        thead=table.append("thead").attr("id","thead_table"),
        tbody=table.append("tbody").attr("id","tbody_table");
        
        thead.append("tr")
              .selectAll("th")
              .data(columns).enter()
              .append("th")
              .attr("class",d=>d)
              .text(d=>d)

    }else{
      thead=table.select("#thead_table"),
      tbody=table.select("#tbody_table");
      tbody.selectAll("tr").remove()                
  
    }
                

          
          tbody.selectAll("tr")
            .data(data)
            .enter()            
            .append("tr")   
            .selectAll("td")
            .data(function(row,i){
              
              return columns.map(column=>{
                var obj={column:column,value:row[column]}
                //
                return obj

              })
            })            
            .enter()
            .append("td")
            //.style("font-family","Roboto")
            .text(d=>{
              
              var text=d.value
              if(d.column!="number" && d.column!="formato"){
                text="$"+(formatNumber(d.value))
              }
             return text
            })


    }
    
    
 
    
    function update_data(newstores){
      

        var minimo=d3.min(newstores,d=>d.venta_rm_total)
        var maximo=d3.max(newstores,d=>d.venta_rm_total)
        //
        var scale_sales=d3.scaleLinear().domain([0,maximo]).range(["red","green"])

        var group=d3.select("#group_in_map")
        
        d3.selectAll(".pin").remove()
        d3.selectAll(".circleNumbers").remove()

        circles=group.selectAll(".pin")
						  .data(newstores)

        //
        
        circles.enter().append("circle")
        .attr("class","pin")
        .on('mouseover',circle_mouseOver)
        .on('mouseout',circle_mouseOver_off) 
        .attr("r", 10)
        //.attr("transform",d=>d!=null?"translate(" + projection([d.location.longitude,d.location.latitude]) + ")":"translate("+projection(["-107.39741","24.80871"])+")")
        //.attr("transform","translate(" + projection(["-107.39741","24.80871"]) + ")")
        .style("fill","white")
        .transition()
        .duration(3000)
        .ease(d3.easeExp,8)
        //.attr("r", 10)        
        .style("fill",d=>scale_sales(d.venta_rm_total))
        .attr("transform",d=>d!=null?"translate(" + projection([d.location.longitude,d.location.latitude]) + ")":"translate("+projection(["-107.39741","24.80871"])+")")

        

        circles.enter()
        .append("text")
        .text(d=>d.numberStores)
        .attr('class','circleNumbers')
        .attr("transform","translate(" + projection(["-107.39741","24.80871"]) + ")")
        .attr("style","font-size:5px")   
           .transition()
           .duration(3000)
           .ease(d3.easeExp,8)
        .attr("style","font-size:10px")   
        .attr("transform",d=>{
          let projectionArray=projection([d.location.longitude,d.location.latitude])
          return "translate("+[(projectionArray[0]-5),projectionArray[1]+5]+")"
          })  

    }

    

    function circle_mouseOver(d){
      
        circle_toolTip.style.display='flex'
        var coords=[d3.event.pageX,d3.event.pageY]       
        circle_toolTip.style.top=(coords[1])+"px"
        circle_toolTip.style.left=(coords[0])+"px"

        var data_stores=d.formatStores
        var info=[]

         

        Object.keys(data_stores).forEach(key => {
          //console.log(key, data_stores[key]);
          var datom=data_stores[key]
          datom.formato=key

          info.push(datom)
        });

         
        if(info!=null&&info.length)
        build_miniTable(info)
        //state_name.innerHTML=d.properties.state_name
        //console.log(state_name.textContent)


    }

    function circle_mouseOver_off(){
        
        circle_toolTip.style.display='none'
        var coords=d3.mouse(this)        
        circle_toolTip.style.top=(coords[1]+15)+"px"
        circle_toolTip.style.left=(coords[0]+15)+"px"
        //state_name.innerHTML=d.properties.state_name
        //console.log(state_name.textContent)

    }
                     
    function zoomHandler(e){
      
        var transform=d3.event.transform

        g.transition().duration(100).attr("transform","translate("
                +transform.x+","+transform.y+")")
                scale("+transform.k+")
                
        g.selectAll("circle")//.attr("r",(1-transform.k/10))        
      }                 



  
  var mapData=[]
  function update_map(direction){

    var date_data=document.getElementById("calendar_data")
    //stores={}

    if(direction){
        console.log("---->")
        if(month_index<13){
            month_index++
            
        }else{
            month_index=1
            year_index++
        }


        var data=sales[year_list[year_index]]
        newstores=data[month_index].stores
        
        
        date_data.innerHTML="01/"+month_index+"/"+year_list[year_index]
        //
        if(newstores!=null){
            
            mapData=[]
            
                       
            Object.keys(newstores).forEach(key=>{              
                var store=newstores[key]
  
                    if(states[store.id_estado]){

                        state=states[store.id_estado]
                    }else{
                        state=states[store.id_estado]={}
                        state.fecha=store.fecha
                        state.nombre_estado=store.nombre_estado
                        state.id_estado=store.id_estado
                        //
                        state.location=store.location
                        state.venta_muebles_total=0
                        state.venta_muebles_credito=0
                        state.venta_muebles_contado=0
                        state.venta_ropa_total=0
                        state.venta_ropa_credito=0
                        state.venta_ropa_contado=0     
                        state.venta_rm_total=0
                        state.numberStores=0
                        state.formatStores={}
                        }

                        
                        var venta_muebles_total=store.venta_muebles_total!=null?store.venta_muebles_total:01
                        var venta_muebles_credito=store.venta_muebles_credito!=null?store.venta_muebles_credito:01
                        var venta_muebles_contado=store.venta_muebles_contado!=null?store.venta_muebles_contado:01
                        var venta_ropa_total=store.venta_ropa_total!=null?store.venta_ropa_total:0
                        var venta_ropa_credito=store.venta_ropa_credito!=null?store.venta_ropa_credito:0
                        var venta_ropa_contado=store.venta_ropa_contado!=null?store.venta_ropa_contado:0
                        

                        state.venta_muebles_total+=venta_muebles_total
                        state.venta_muebles_credito+=venta_muebles_credito
                        state.venta_muebles_contado+=venta_muebles_contado

                        state.venta_ropa_total+=venta_ropa_total
                        state.venta_ropa_credito+=venta_ropa_credito
                        state.venta_ropa_contado+=venta_ropa_contado

                        state.venta_rm_total+=state.venta_ropa_total+state.venta_muebles_total
                        state.venta_rm_credito+=state.venta_ropa_credito+state.venta_muebles_credito
                        state.venta_rm_contado+=state.venta_ropa_contado+state.venta_muebles_contado
                        
                        state.numberStores++
                        
                        debugger
                        
                        if(state.formatStores[store.formato_tienda]){
                          var state_storeKind=state.formatStores[store.formato_tienda]
                          state_storeKind.number++
                          state_storeKind.venta_muebles_total+=venta_muebles_total
                          state_storeKind.venta_muebles_credito+=venta_muebles_credito
                          state_storeKind.venta_muebles_contado+=venta_muebles_contado

                          state_storeKind.venta_ropa_total+=venta_ropa_total
                          state_storeKind.venta_ropa_credito+=venta_ropa_credito
                          state_storeKind.venta_ropa_contado+=venta_ropa_contado

                          state_storeKind.venta_rm_total+=venta_ropa_total+venta_muebles_total
                          state_storeKind.venta_rm_credito+=venta_ropa_credito+venta_muebles_credito
                          state_storeKind.venta_rm_contado+=venta_ropa_contado+venta_muebles_contado
                          
                        
                        }else{

                          state.formatStores[store.formato_tienda]={
                            number:1,
                            venta_muebles_total:venta_muebles_total,
                            venta_muebles_credito:venta_muebles_credito,
                            venta_muebles_contado:venta_muebles_contado,
                            venta_ropa_total:venta_ropa_total,
                            venta_ropa_credito:venta_ropa_credito,
                            venta_ropa_contado:venta_ropa_contado,
                            venta_rm_total:venta_ropa_total+venta_muebles_total,
                            venta_rm_credito:venta_ropa_total+venta_muebles_total,
                            venta_rm_contado:venta_ropa_total+venta_muebles_total
                        
                          
                          }
                        }
                        
                        
                
                
            })//For
            //
            if(states!=null){
                for (var [key, value] of Object.entries(states)) {
                    
                    //console.log(value.id_estado+" "+value.venta_rm_total);
                    if(value!=null)
                    mapData.push(value) 
                }
                update_data(mapData)
            }
        }
        
    }else{
        console.log("<----")
    }
  }

  function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  //*********************************-------------------------------------------------*********************************/
  //Time Slider

  var formatDateIntoYear=d3.timeFormat("%Y")
  var formateDate=d3.timeFormat("%b/%Y")
  var parseDate=d3.timeParse("%m/%d/%Y")

  //YYYY MM DD
  var startDate=new Date("2016-01-01")
  var endDate=new Date("2020-06-01")
  var actual_date

  var margin_slider={top:50,bottom:0,left:25,right:25},
      width=500 - margin_slider.left - margin_slider.right
      height=200 - margin_slider.top - margin_slider.bottom
  
var svg= d3.select("#time_slider_map")
        .append("svg")
        .attr("width",width + margin_slider.left + margin_slider.right)
        .attr("height", height + margin_slider.top + margin_slider.bottom)

 var moving=false
 var currentValue=0
 var targetValue=width

 var playButton=d3.select("#play_slider_button")
      .on("click",function(){
        
        var button=d3.select(this)
        if(moving){
          moving=false
          clearInterval(timer)
          button.text("Play")
        }else{
          moving=true
          timer=setInterval(step,3000)
          button.text("Pause")
        }
      })

 var x=d3.scaleTime()
        .domain([startDate,endDate])
        .range([0,targetValue])
        .clamp(true)

 var slider=svg.append("g")
            .attr("class","slider_time")
            .attr("transform","translate("+margin_slider.left+","+height/5+")")

            slider.append("line")
                  .attr("class","track")
                  .attr("x1",x.range()[0])
                  .attr("x2",x.range()[1])
                  .select(function(){
                    return this.parentNode.appendChild(this.cloneNode(this))
                  })
                  .attr("class","track-insect")
                  .select(function(){
                    return this.parentNode.appendChild(this.cloneNode(this))
                  })
                  .attr("class","track-overlay")
                  .call(d3.drag()
                  .on("start.interrupt",function(){
                    slider.interrupt()
                  })
                  .on("start drag",function(){
                    currentValue=d3.event.x
                    update_handle_data(x.invert(currentValue))
                  })
                  )

slider.insert("g",".track-overlay")
                  .attr("class","ticks")
                  .attr("transform","translate(0,"+18+")")
                  .selectAll("text")
                  .data(x.ticks(4))
                  .enter()
                  .append("text")
                  .attr("x",x)
                  .attr("y",10)
                  .attr("text-anchor","middle")
                  .text(function(d){return formatDateIntoYear(d)})
                  

var handle=slider.insert('circle','.track-overlay')
                  .attr("class",'handle')
                  .attr("r",9)

var label=slider.append("text")                  
                  .attr("class","label")
                  .attr("text-anchor","middle")
                  .text(formateDate(startDate))
                  .attr("transform","translate(0,"+(-15)+")")

var timer=setInterval(step,3000);
moving=true

function update_handle_data(handle_circle){
  handle.attr("cx",x(handle_circle))

  label.text(formateDate(handle_circle))
       .attr("x",x(handle_circle))
       var year,month
/*
       year=handle_circle.getFullYear()
       month=handle_circle.getMonth()
       if(month!=month_index){
         month_index=month
         year=year_index
         update_map(true)
       }
       
*/         

} 


function step(){
  update_handle_data(x.invert(currentValue))
  console.log(currentValue)
  currentValue=currentValue+(targetValue/151)
  //
  if(currentValue>targetValue){
    moving=false
    currentValue=0
    clearInterval(timer)
    timer=0
    playButton.text("Play")
    //alert("Slider en moviemiento: "+moving)
    

  }else{
    
    //update_data(true)
    
    
  }
}