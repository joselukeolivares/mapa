function barChart(){

    var _chart={}

    var _width=650,_height=300,
        _margins={top:30,left:80,right:30,bottom:30},
        _x,
        _y,
        _data=[],
        _colors=d3.scaleOrdinal(d3.schemeCategory10),
        _svg,
        _bodyG
        

        _chart.render=function(){

            if(!_svg){
                _svg=d3.select("#bar_chart").append("svg")
                    .style("margin","auto")
                    .attr("width",_width)
                    .attr("height",_height)

                    renderAxes(_svg)
                    defineBodyClip(_svg)
            }

            renderBody(_svg)

        }

        function renderAxes(svg){
                var axesG=svg.append("g")
                        .attr("class","axes")

                var xAxis=d3.axisBottom()
                    .scale(_x.range([0,quadrantWidth()])) 
                    
                var yAxis=d3.axisLeft()
                    .scale(_y.range([quadrantHeight(),0]))
                    
                axesG.append("g")
                .attr("class","axis")
                .attr("transform",function(){
                    return "translate(" +xStart()+","+yStart()+")"
                })
                .call(xAxis)

                axesG.append("g")
                .attr("class","axis")
                .attr("transform",function(){
                    return "translate("+xStart()+","+yEnd()+")"
                })
                .call(yAxis)

        }

        function defineBodyClip(svg){
            var padding=5

            svg.append("defs")
                .append("clipPath")
                .attr("id","body-clip")
                .append("rect")
                .attr("x",0)
                .attr("y",0)
                .attr("width",quadrantWidth()+2*padding)
                .attr("height",quadrantHeight()+2*padding)
        }

        function renderBody(svg){
            if(!_bodyG){
                _bodyG=svg.append("g")
                        .attr("class","body")
                        .attr("transform","translate("
                        +xStart()
                        +","
                        +yEnd()
                        +")")
                        .attr("clip-path","url(#body-clip)");
            }

            renderBars()
        }

        function renderBars(){
            var padding =2

            var bars=_bodyG.selectAll("rect.bar")
                    .data(_data)

                
                
                bars.enter()
                    .append("rect")
                    .merge(bars)
                    .attr("class","bar")
                    .transition()
                    .attr("x",d=>_x((d.date)))
                    .attr("y",d=>_y(d.venta_rm_total))
                    .attr("height",d=>yStart()-_y(d.venta_rm_total))
                    .attr("width",d=>10)//Math.floor(quadrantWidth()/_data.length)-1)

        }

        function xStart(){
            return _margins.left
        }

        function yStart(){
            return _height - _margins.bottom
        }

        function xEnd(){
            return _width- _margins.right
        }

        function yEnd(){
            return _margins.top
        }

        function quadrantWidth(){
            return _width - _margins.left - _margins.right
        }

        function quadrantHeight(){
            return _height - _margins.top - _margins.bottom
        }

        _chart.height=function(h){
            if(!arguments.length)return _height
            _height=h
            return _chart
        }

        _chart._width=function(w){
            if(!arguments.length)return _width
            _width=w
            return _chart
  
        }

        _chart.margins=function(m){
            if(!arguments.length)return _margins
            _margins=m
            return _chart
        }

        _chart.colors=function(c){
            if(!arguments.length)return _colors
            _colors=c
            return _chart
        }

        _chart.x=function(x){
            if(!arguments.length)return _x
            _x=x
            return _chart
        }

        _chart.y=function(y){
            if(!arguments.length)return _y
            _y=y
            return _chart
        }

        _chart.setData=function(data){
            _data=data
            return _chart
        }

        _chart.fake_month=1
        _chart.fake_year=2016
        function randomData(){
            var obj={date:`31/${_chart.fake_month}/${_chart.fake_year}`,sales:random()*1000000}

            if(_chart.fake_month==12){
                _chart.fake_month=1
                _chart.fake_year++
            }

        
        

         return obj
        }


                      
        

  return _chart
        
        
}






var barData=[]


       

function update(){
            var year,month
                 Object.keys(sales).forEach(key=>{
                 var obj=sales[key]
                 year=key
                 
                 
                Object.keys(obj).forEach(key=>{

                        if(key!="stores"){
                            month=key
                            var datum=obj[key]
                            barData.push({date:new Date(month+"/1"+"/"+year),
                                venta_ropa_total:datum.venta_ropa_total,
                                venta_ropa_credito:datum.venta_ropa_credito,
                                venta_ropa_contado:datum.venta_ropa_contado,
                                venta_muebles_total:datum.venta_muebles_total,
                                venta_muebles_credito:datum.venta_muebles_credito,
                                venta_muebles_contado:datum.venta_muebles_contado,
                                venta_rm_total:datum.venta_ropa_total+datum.venta_muebles_total,
                                venta_rm_credito:datum.venta_ropa_credito+datum.venta_muebles_credito,
                                venta_rm_contado:datum.venta_ropa_contado+datum.venta_muebles_contado

                            })

                            
                
                        }
                })

            
            
            })

            
            
        }


function buid_barChart(){
    update()
    
    var maximo=d3.max(barData,d=>d.venta_rm_total)
    var time_max=d3.max(barData,(d)=>new Date(d.date))
    var time_min=d3.min(barData,(d)=>new Date(d.date))

            var chart=barChart()
            .setData(barData)
            .x(d3.scaleTime().domain([time_min,time_max]))
            .y(d3.scaleLinear().domain([0,maximo]))

        
    
            //debugger
            
            chart.render()

                /*
                            // Create slider spanning the range from 0 to 10
                var slider = createD3RangeSlider(0, 250, "#slider-container");

                // Range changes to 3-6
                slider.range(3,6);

                // Listener gets added
                slider.onChange(function(newRange){
                console.log(newRange);
                });

                // Range changes to 7-10
                // Warning is printed that you attempted to set a range (8-11) outside the limits (0-10)
                // "{begin: 7, end: 10}" is printed in the console because of the listener
                slider.range(8);

                // Access currently set range
                var curRange = slider.range();

                // "7-10" is written to the current position in the document
                console.log(curRange.begin + "-" + curRange.end)
                */


                  // Range
                    var sliderRange = d3
                    .sliderBottom()
                    .min(time_min)
                    .max(time_max)
                    .width(300)
                    .tickFormat(d3.timeFormat('%b%Y'))
                    .ticks(5)
                    .default([time_min, time_max])
                    .fill('#2196f3')
                    .on('onchange', val => {

                        
                        
                        d3.select('p#value-range').text(val.map(d3.timeFormat('%b/%d/%Y')).join('-'));
                        var timeParse=d3.timeFormat('%b/%d/%Y')
                       var min=timeParse(new Date(val[0]))
                       var max=timeParse(new Date(val[1]))
                        
                       if( typeof val[0]==="Date" && typeof val[1]==="Date"){
                           //debugger
                       }else{
                           var obj=typeof val[0]
                           //debugger
                       }
                        var subData=barData.filter(d=>{
                            console.log(time_min+":"+min)
                            return d.date<=max && d.date>=min
                        })
                        //debugger
                        //chart.x(d3.scaleTime().domain([min,max]))
                        chart.setData(subData)
                        chart.render()
                        console.log(min+":"+min)
                        console.log(max+":"+max)
                        console.log(subData)
                        
                        

                        
                    });

                    
                    

                    var gRange = d3
                    .select('div#slider-range')
                    .append('svg')
                    .attr('width', 500)
                    .attr('height', 100)
                    .append('g')
                    .attr('transform', 'translate(30,30)');

                    gRange.call(sliderRange);

                    d3.select('p#value-range').text(
                    sliderRange
                        .value()
                        .map(d3.timeFormat('%b%Y'))
                        .join('-')
                    );

}        
